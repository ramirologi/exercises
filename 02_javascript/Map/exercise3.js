function mostTimes (arr) {

	var keys = [];

	arr.forEach(function (item) {
		if(!keys.includes(item)) {
			keys.push(item);
		}
	})

	var times = [];
	var count = 0;

	keys.forEach(function(unique) {
		arr.forEach(function(repeat) {
			if(unique === repeat) {
				count++;
			}
		})
		times.push(count);
		count = 0;
	})

	var map = new Map();

	keys.forEach(function(pair, index) {
		map.set(pair, times[index])
	})	

	return map;

}

module.exports = function () {
}