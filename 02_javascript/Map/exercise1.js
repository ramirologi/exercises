function objToMap (objProp, objSchema) {

	var arrEntries = Object.entries(objProp);
	var arrProp = Object.keys(objProp);
	var arrSchema = Object.keys(objSchema);


	var levelOne = [];
	arrProp.forEach(function(key, index) {
		if(arrSchema.includes(key)) {
			levelOne.push(arrEntries[index]);	
		}
	})
	 

	var values = Object.values(objProp);
	var levelTwo = [];
	values.forEach(function(value, index) {
		if(!isNaN(Number(value))) {
			levelTwo.push(arrEntries[index]);
		}
	})


	var finalArr = [];
	levelOne.forEach(function(pair) {
		if(levelTwo.includes(pair)) {
			finalArr.push(pair);
		}
	})

	var map = new Map();

	finalArr.forEach(function(mapItem) {
		map.set(mapItem[0],mapItem[1])
	})

	return map;

}

module.exports = function ()  {}
