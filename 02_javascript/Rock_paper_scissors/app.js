let userScore = 0;
let computerScore = 0;
const userScoreSpan = document.getElementById("user-score");
const computerScoreSpan = document.getElementById("computer-score");
const scoreBoardDiv = document.querySelector(".score-board");
const resultP = document.querySelector(".result > p");
const rockDiv = document.getElementById("r");
const paperDiv = document.getElementById("p");
const scissorsDiv = document.getElementById("s");

function getComputerChoice () {
	const choices =["r","p","s"];
	const randomNumber = Math.floor(Math.random() * 3); 
	return choices[randomNumber];
}

function convertToWord (letter) {
	if (letter === "r") return "Rock";
	if (letter === "p") return "Paper";
	return "Scissors";
}

function win (userChoice, computerChoice) {
	userScore++;
	userScoreSpan.innerHTML = userScore;
	computerScoreSpan.innerHTML = computerScore;
	resultP.innerHTML = convertToWord(userChoice) + " beats "+ convertToWord(computerChoice) + ". You win!";
}

function lose (userChoice, computerChoice) {
	computerScore++;
	computerScoreSpan.innerHTML = computerScore;
	userScoreSpan.innerHTML = userScore;
	resultP.innerHTML = convertToWord(userChoice) + " loses to "+ convertToWord(computerChoice) + ". You lost...";
}

function draw (userChoice, computerChoice) {
	resultP.innerHTML = "It's a draw!";
}

function game (userChoice) {
	const computerChoice = getComputerChoice();
	switch (userChoice + computerChoice) {
		case "rs":
		case "pr":
		case "sp":
		win(userChoice, computerChoice);
		break;
		case "rp":
		case "ps":
		case "sr":
		lose(userChoice, computerChoice);
		break;
		case "rr":
		case "pp":
		case "ss":
		draw(userChoice, computerChoice);
		break;
	}
}

function main () {

	if (userScore === 2 || computerScore === 2) {
		userScore = 0;
		computerScore = 0;
	}

rockDiv.addEventListener('click', function () {
	game("r")
})

paperDiv.addEventListener('click', function () {
	game("p")
})

scissorsDiv.addEventListener('click', function () {
	game("s")
})

}

main();