function numberConverter (arr) {
	var converted = 0;
	var nonConverted = 0;
	var convertedItems = [];

	arr.forEach(function (item) {
		if (typeof item === "string") {
			var num = Number(item);
			if (isNaN(num) === false) {
				convertedItems.push(num);
				converted = converted + 1;
			} else {
				nonConverted = nonConverted + 1;
			}
		} else if (typeof item === "object" || typeof item === "function") {
			nonConverted = nonConverted + 1;
		}
	})

	var numbers = 0;

	arr.forEach(function (ele) {
		if (typeof ele === "number") {
			numbers = numbers + 1;
		}
	})

	if (numbers === arr.length) {
		return "no need for conversion";
	} else { 
		return `${converted} were converted to numbers: ${convertedItems}; ${nonConverted} couldn't be converted`;
	} 
}

debugger;
numberConverter([{},"22",()=>{},44,"44","88"])

module.exports ={
    numberConverter
}




