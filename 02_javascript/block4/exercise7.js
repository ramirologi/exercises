function boolanChecker (arr, maxCapacity) {
	var bool = [];

	if (maxCapacity == undefined) {
		arr.forEach(function (item) {
			if (typeof item === "boolean") {
				bool.push(item);
			}
		})
	} else {
		for (var i = 0; bool.length < maxCapacity; i++) {
			if (typeof arr[i] === "boolean") {
				bool.push(arr[i]);
			}
		}
	}
	return `${bool.length} booleans were found ${bool}`;
}

module.exports = {
    boolanChecker
}

