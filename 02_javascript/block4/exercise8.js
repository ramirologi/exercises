function uniqueElements (arr) {
	var unique = [];

	for (var i = 0; i < arr.length; i++) {
		if (!unique.includes(arr[i])) {
			unique.push(arr[i]);
		}
	}

	return `old array ${arr} \n new array ${unique}`;
}

   
module.exports = {
    uniqueElements
}