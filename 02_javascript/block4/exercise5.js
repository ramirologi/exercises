function howManyCaps (str) {

	var arr = [];

	for (var i = 0; i < str.length; i++) {
		if (str[i] === "A" || str[i] === "B" || str[i] === "C" || str[i] === "D" || str[i] === "E" || str[i] === "F" || str[i] === "G" || str[i] === "H" || str[i] === "I" || str[i] === "J" || str[i] === "K" || str[i] === "L" || str[i] === "M" || str[i] === "N" || str[i] === "O" || str[i] === "P" || str[i] === "Q" || str[i] === "R" || str[i] === "S" || str[i] === "T" || str[i] === "U" || str[i] === "V" || str[i] === "W" || str[i] === "X" || str[i] === "Y" || str[i] === "Z") {
			arr.push(str[i]);
		}
	}

	var length = arr.length;
	return "There are " + length + " capitals and these are " + arr;

}

module.exports = {
    howManyCaps
}