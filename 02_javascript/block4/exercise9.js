function uniqueElements (arr, max) {
	var unique = [];

	for (var i = 0; i < arr.length; i++) {
		if (!unique.includes(arr[i]) && arr[i] >= max && typeof arr[i] === "number") {
			unique.push(arr[i]);
		}
	}

	return `old array ${arr} \n new array ${unique}`;
}

debugger;
uniqueElements(["a","b","b","44",55,4,4],25);

module.exports = {
    uniqueElements
}