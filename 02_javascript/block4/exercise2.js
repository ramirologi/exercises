function is_an_even_number (arr) {
	var count = 0;
	for (var i = 0; i < arr.length; i++) {
		var par = arr[i];
		var parse = parseInt(par, 10);
		if (typeof parse == "number" && parse % 2 == 0) {
			count = count + 1;
		}	
	}
	return count;
}

module.exports = {
    is_an_even_number
}