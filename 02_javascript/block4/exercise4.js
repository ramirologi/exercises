function checker (str) {

	var commas = 0;
	var questions = 0;

	for (var i = 0; i < str.length; i++) {
		if (str[i] === ",") {
			commas++;
		} else if (str[i] === "?") {
			questions++;
		}
	}

	if (commas < 2 && questions < 2) {
		return commas + " comma, " + questions + " question mark";
	} else if (commas < 2 && questions !== 1) {
		return commas + " comma, " + questions + " question marks";
	} else if (commas !== 1 && questions < 2) {
		return commas + " commas, " + questions + " question mark";
	} else {
		return commas + " commas, " + questions + " question marks";
	};
	

}

module.exports = {
    checker
}



