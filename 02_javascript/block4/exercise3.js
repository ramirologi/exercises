function check_types (arr) {
	var count = 0;
	var arr2 = [];

	for (var i = 0; i < arr.length; i++) {
		arr2.push(typeof arr[i]);
	};

	if (arr2.includes("string")) {
			count = count + 1;
	};

	if (arr2.includes("number")) {
			count = count + 1;
	};

	if (arr2.includes("object")) {
			count = count + 1;
	};

	if (arr2.includes("function")) {
			count = count + 1;
	};

	if (arr2.includes("boolean")) {
			count = count + 1;
	};

	if (arr2.includes("undefined")) {
			count = count + 1;
	};

	return count;
}


module.exports ={
    check_types
}