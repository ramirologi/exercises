function isTrue (arg) {
	if (arg === undefined || arg === "" || arg === false || arg === null || arg === 0) {
		return false;
	} else {
		return true;
	}
}

module.exports = {
    isTrue
}