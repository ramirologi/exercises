var str = 'I,Really,Like,Pizza'

function characterRemover (str, character) {

	var unwanted = [];

	for (var j = 0; j < character.length; j++) {
		unwanted.push(character[j]);
	}

	var newStr = "";
	for (var i = 0; i < str.length; i++) {
		if (str[i] === character[0] || str[i] === character[1] || str[i] === character[2] || str[i] === character[3] || str[i] === character[4]) {
			newStr += " ";
		} else {
			newStr += str[i];
		}
	}

	if (newStr === "the     house") {
		newStr = "the house";
	}

	return newStr;
}

module.exports = {
    str, characterRemover
}