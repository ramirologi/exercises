var arr  =  ['banana','apple','orange'];
var arr2 =  ['tv','dvd-player','playstation']; 
var pos  =  2;

function swap (arr, arr2, pos) {
	var removeArr = arr[pos]; 
	var removeArrTwo = arr2[pos];
	arr.splice(pos, 1, removeArrTwo);
	arr2.splice(pos, 1, removeArr); 
	return String([arr, arr2])
}

module.exports ={
    arr, arr2,pos, swap
}