function isArrayFunc (arg) {
	var check = arg[0];
	if(check === undefined && typeof arg !== "string") {
		return false;
	} else {
		return true;
	}
}

module.exports = {
    isArrayFunc
}
