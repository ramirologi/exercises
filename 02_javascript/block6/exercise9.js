function sumAll (obj) {
	var sum = 0;
	if (obj !== NaN && obj !== undefined && obj !== {}) {
		for (var key in obj) {
			sum += obj[key];
		}
	}	
	return sum;
}


module.exports = {
    sumAll
}