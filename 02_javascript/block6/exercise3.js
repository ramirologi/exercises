var obj = {a: 1, b: 2};

function modifyObject (obj, key, value) {
	obj[key] = value;
	return obj;
}

module.exports ={
    obj, modifyObject
}