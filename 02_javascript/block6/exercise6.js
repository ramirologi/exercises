function getIndex (arr, key, value) {
	var index = -1;
	for(var i = 0; i < arr.length; i++){
       if( arr[i][key] === value){
       	  index = i;
       	  break
       }
	}
	return index;
}

module.exports ={
    getIndex
}

 