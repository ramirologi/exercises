function last (obj) {

	var newObj = {};

	var prop = Object.keys(obj);

	var propLength = prop.length - 1;

	newObj[prop[propLength]] = obj[prop[propLength]];

	return newObj;

}

module.exports ={
    last
}
