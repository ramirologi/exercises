function splice (obj, start, items) {

	if(items === undefined) {
		items = 1;
	}

	var arr = Object.entries(obj);
	var newArr = [];
	for (var i = start; i < arr.length; i++) {
		newArr.push(arr[i]);
	}
	
	var newObj = {};
	newArr.forEach(function (item) {
		newObj[item[0]] = item[1];
	})

	return newObj;

}

module.exports ={
    splice
}