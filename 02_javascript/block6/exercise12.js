function model (action, object, schema) {
	var DB = [];
	var newObj = {};

	var objectEntries = Object.entries(object);
	var schemaEntriesOne = Object.entries(schema);

// es el schema todo en arraies.

if (action === "add") {

	var schemaEntriesTwo = [];

	for (var i = 0; i < schemaEntriesOne.length; i++) {	
	schemaEntriesTwo.push(schemaEntriesOne[i][0]);
	var index = Object.entries(schemaEntriesOne[i][1]);
	schemaEntriesTwo.push(index);
	}

// son las key que se aceptan
	var okProperties = [];

	for (var j = 0; j < schemaEntriesTwo.length; j++){
		if (typeof schemaEntriesTwo[j] === "string") {
			okProperties.push(schemaEntriesTwo[j]);
		}
	}

// son los pares del object que están incluidos en los aceptados por schema.
	var objectEntriesTwo = [];

	for (var k = 0; k < objectEntries.length; k++) {
		if (okProperties.includes(objectEntries[k][0])) {
			objectEntriesTwo.push(objectEntries[k]);
		}
	}

// pairs con id y type ok.
	var objectEntriesThree = [];

	for(var l = 0; l < objectEntriesTwo.length; l++){
		if (objectEntriesTwo[l][0] === "name" && typeof objectEntriesTwo[l][1] === "string") {
			objectEntriesThree.push(objectEntriesTwo[l]);
		} else if (objectEntriesTwo[l][0] === "id" && typeof objectEntriesTwo[l][1] === "number") {
			objectEntriesThree.push(objectEntriesTwo[l]);
		} else if (objectEntriesTwo[l][0] === "age" && typeof objectEntriesTwo[l][1] === "number") {
			objectEntriesThree.push(objectEntriesTwo[l]);
		} else if (objectEntriesTwo[l][0] === "married" && typeof objectEntriesTwo[l][1] === "boolean") {
			objectEntriesThree.push(objectEntriesTwo[l]);
		}
	}

// cuando no está en object y tiene default

	var objectEntriesThreeKeys = [];

	for (var o = 0; o < objectEntriesThree.length; o++) {
		objectEntriesThreeKeys.push(objectEntriesThree[o][0]);
	}

	var objectEntriesFour = [];

	for (var p = 0; p < objectEntriesThree.length; p++){
		if(!objectEntriesThreeKeys.includes("name")) {
			objectEntriesFour.push("name","NoBody");
		} else if(!objectEntriesThreeKeys.includes("married")) {
			objectEntriesFour.push("married",false);
	}
	}

	objectEntriesThree.push(objectEntriesFour);

	var newObj = {};

	objectEntriesThree.forEach(function (arr) {
		newObj[arr[0]] = arr[1];
	})

	}

	DB.push(newObj);
	return DB;
}


module.exports = {
    model
}