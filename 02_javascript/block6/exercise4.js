var movies = ['matrix','the dark knight','a beautiful mind','american pie'];

function addToList (movies) {
	var movieList = [];
	movies.forEach(function (item, index) {
		var obj = {};
		obj["title"] = movies[index];
		obj["id"] = index;
		movieList.push(obj);
	})
	return movieList;
}

module.exports = {
    addToList
}