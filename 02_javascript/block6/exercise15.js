function sort (obj) {

	var arr = Object.entries(obj);
//[["a", 1],["b", 20],["c", 3],["d", 4],["e", 1],["f", 4]]

	var objValues = Object.values(obj);
//[[1],[20],[3],[4],[1],[4]]

	function comparar (a,b) {
		return a-b;
	}
	var ordered = objValues.sort(comparar);
//[[1],[1],[3],[4],[4],[20]]

	var newArr = [];

	for (var i = 0; i < ordered.length; i++) {
		for (var j = 0; j < arr.length; j++) {
			if (ordered[i] === arr[j][1]) {
				newArr.push(arr[j]);
				arr.splice(j,1);
				break;
			}
		}
	}

	var newObj = {};

	newArr.forEach(function (item) {
		newObj[item[0]] = item[1];
	})

	return newObj;
}


module.exports ={
    sort
}
