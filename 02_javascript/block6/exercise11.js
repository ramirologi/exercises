function model (action, object, schema) {
	var DB = [];
	var newObj = {};

	var objectEntries = Object.entries(object);
	var schemaEntries = Object.entries(schema);

	var allowedPairs = [];
	
	if (action === "add") {
		
		for (var i = 0; i < objectEntries.length; i++) {
			for (var j = 0; j < schemaEntries.length; j++) {
				if (schemaEntries[j].includes(objectEntries[i][0]) && typeof objectEntries[i][1] === schemaEntries[j][1]) {
					allowedPairs.push(objectEntries[i]);
				}
			}
		}
	}

	allowedPairs.forEach(function (arr) {
		newObj[arr[0]] = arr[1];
	})

	DB.push(newObj);
	return DB;
}

module.exports = {
    model
}





