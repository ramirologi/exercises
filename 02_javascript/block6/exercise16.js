function sort (obj, kOrV, order) {

	var arr = Object.entries(obj);
//[["a", 1],["b", 20],["c", 3],["d", 4],["e", 1],["f", 4]]

	var objKeys = Object.keys(obj);
//[["a"],["b"],["c"],["d"],["e"],["f"]]

	var objValues = Object.values(obj);
//[[1],[20],[3],[4],[1],[4]]

	function comparar (a,b) {
		return a-b;
	}
	var orderedValues = objValues.sort(comparar);
//[[1],[1],[3],[4],[4],[20]]

	var orderedKeys = objKeys.sort();
//[["a"],["b"],["c"],["d"],["e"],["f"]]

	var newArr = [];


	if (kOrV === "values" && order === "ascending") {

		for (var i = 0; i < orderedValues.length; i++) {
			for (var j = 0; j < arr.length; j++) {
				if (orderedValues[i] === arr[j][1]) {
					newArr.push(arr[j]);
					arr.splice(j,1);
					break;
				}
			}
		}
	} else if (kOrV === "keys" && order === "ascending") {

		for (var i = 0; i < orderedKeys.length; i++) {
			for (var j = 0; j < arr.length; j++) {
				if (orderedKeys[i] === arr[j][0]) {
					newArr.push(arr[j]);
					arr.splice(j,1);
					break;
				}
			}
		}
	} else if (kOrV === "values" && order === "descending") {

		for (var i = orderedValues.length; i >= 0; i--) {
			for (var j = 0; j < arr.length; j++) {
				if (orderedValues[i] === arr[j][1]) {
					newArr.push(arr[j]);
					arr.splice(j,1);
					break;
				}
			}
		}
	} else if (kOrV === "keys" && order === "descending") {

		for (var i = orderedKeys.length; i >= 0; i--) {
			for (var j = 0; j < arr.length; j++) {
				if (orderedKeys[i] === arr[j][0]) {
					newArr.push(arr[j]);
					arr.splice(j,1);
					break;
				}
			}
		}
	} else {
		return "missing argument here!";
	}

	var newObj = {};

	newArr.forEach(function (item) {
		newObj[item[0]] = item[1];
	})

	return newObj;

}

module.exports ={
    sort
}