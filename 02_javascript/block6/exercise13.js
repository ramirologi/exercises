var bankAccount = {

total: 0,

withdraw: function (egreso) {
	bankAccount.total -= egreso;
},

deposit: function (ingreso) {
	bankAccount.total += ingreso;
},

balance: function () {
	return bankAccount.total;
}

}

module.exports = {
    bankAccount
}