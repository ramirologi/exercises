function runOnRange (obj) {

	var arr = [];

	var start = obj["start"];
	var end = obj["end"];
	var step = obj["step"];

	if (start < end) {
		if (step !== 0) {
			for (var i = start; i <= end; (i = i + step)) {
			arr.push(i);
			}
		}
	} else {
		if (step < 0) {
			for (var j = start; j >= end; (j = j + step)) {
			arr.push(j);
			}
		}
	}

	return arr;
	
}


module.exports = {
    runOnRange
}