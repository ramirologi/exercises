function multy (arr) {
	var mul = 1;
	for (var i = 0; i < arr.length; i++) {
		mul *= arr[i];
	}
	return mul;
}

module.exports ={
    multy
}