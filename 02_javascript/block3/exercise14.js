function budgetTracker (arr) {
	var total = 0;

	for (var i = 0; i < arr.length; i++) {
		var str = arr[i];
		var num = Number(str);
		total += num;
	}

	var average = total / arr.length * 0.0089;

	return average.toFixed(0);
}

module.exports = {
    budgetTracker
}
