var arr = ['one','two','three','four'];

function looper (arr) {
	var count = 0;
	arr.forEach(function (item, index) {
		console.log(item, index);
		count = count + 1;
	})
	return count;
}


module.exports = { 
    looper, arr
}