function reverser (str) {
	var strTwo = "";
	for (var i = (str.length - 1); i > -1; i--) {
		strTwo += str[i];
	}
	return strTwo;
}

module.exports = {
    reverser
}
