function sum (arr) {
	var suma = 0;
	for (var i = 0; i < arr.length; i++) {
		suma += arr[i];
	}
	return suma;
}

module.exports ={
    sum
}