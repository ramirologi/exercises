function shortener (str) {
	var firstLetter = "";
	var name = "";
	var surename = "";
	var stop = 0;

	for (var i = 0; i < str.length; i++) {
		if (str[i] === " ") {
			stop = i;
		}
	}

	for (var j = 0; j < stop; j++) {
		if (j === 0) {
			firstLetter += str[j];
		} else {
			name += str[j];
		}
	}

	surename += str[stop + 1];
	

	return firstLetter.toUpperCase() + name + " " + surename.toUpperCase() + ".";
}


module.exports = {
    shortener
}