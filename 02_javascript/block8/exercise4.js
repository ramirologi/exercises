function tally (arr, type) {

if (type !== "arr") {

	var uniques = [];
	var obj = {};

	for (var i = 0; i < arr.length; i++) {
		if(!uniques.includes(arr[i])) {
			uniques.push(arr[i]);
		}
	}

	for (var j = 0; j < uniques.length; j++) {
		var repeat = 0;
		for (var k = 0; k < arr.length; k++) {
			if(uniques[j] === arr[k]) {
				repeat++;
			}
		}
		obj[uniques[j]] = repeat;
		repeat = 0;
	}

return obj;

} else {

	var uniques = [];
	var arr2 = [];

	for (var i = 0; i < arr.length; i++) {
		if(!uniques.includes(arr[i])) {
			uniques.push(arr[i]);
		}
	}

	for (var j = 0; j < uniques.length; j++) {
		var repeat = 0;
		for (var k = 0; k < arr.length; k++) {
			if(uniques[j] === arr[k]) {
				repeat++;
			}
		}
		arr2.push([uniques[j]],repeat);
		repeat = 0;
	}

return arr2;

}

}

module.exports = {
    tally
}