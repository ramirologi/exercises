function sorter (arr, order) {

	function comparar (a,b) {
		return a-b;
	}
	var ordered = arr.sort(comparar);

	if (order === undefined || order === 'ascending') {
		
		return ordered;

	} else if (order === 'descending') {

		var des = [];

		for (var i = (ordered.length -1); i >= 0; i--) {
			des.push(ordered[i]);
		}

		return des;

	} else {

		return "wrong order provided whatever please provide us with ascending or descending order instructions";

	}

}

module.exports ={
    sorter
}