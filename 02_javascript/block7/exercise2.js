var crypt = [];
var includedCoins = [];

var addCurrency = (objCoin, value, crypt) => {

	for (var i = 0; i < crypt.length; i++){
		includedCoins.push(crypt[i]["coin"]);
	}
	if(!includedCoins.includes(objCoin["coin"])){
		crypt.push(objCoin);
		if(objCoin.coin === "bitcoin") {
			return `New coin Bitcoin added to Database`;
		} else if(objCoin.coin === "eth") {
			return `New coin Eth added to Database`;
		} else if (objCoin.coin === "omg") {
			return `New coin Omg added to Database`;
		} else {
			return `New coin ${objCoin.coin} added to Database`;
		}
	} else {
		return findcurrency(objCoin, value)
	}
}

var findcurrency = (objCoin, value) => {
	var convRate = objCoin["rate"];
	return converter(objCoin, value, convRate)
}

var converter = (objCoin, value, convRate) => {
	var convertion = convRate * value;
	return tellConversion(objCoin, value, convRate, convertion);
}

var tellConversion = (objCoin, value, convRate, convertion) => {
	if (objCoin.coin === "bitcoin") {
		return `You will receive ${convertion} usd for your ${value} bitcoins`;
	} else if (objCoin.coin === "eth") {
		return `You will receive ${convertion} usd for your ${value} eths`;
	} else {
		return `You will receive ${convertion} usd for your ${value} ${objCoin.coin}`;
	}
}

module.exports = {
    addCurrency, findcurrency, converter, tellConversion
}