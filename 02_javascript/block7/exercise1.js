var DB = [
    {
        genre:'thriller', 
        movies:[
            {
                title:'the usual suspects', release_date:1999
            }
        ]},
        {
        genre:'commedy', 
        movies:[
            {
                title:'pinapple express', release_date:2008
            }
        ]}
]


function moviesDB (arr, genreName, movieObj) {
	
// Searches if the genre already exists.
	var genreFound = 0;
	arr.forEach(function (genreObject) {
		if (genreObject.genre == genreName) {
				genreFound = genreFound + 1;
		}
	})
// If the genre does not exists: it creats an object with the "genre" and "movies" properties.
		if (genreFound === 0) {
			var newObj = {};
			newObj["genre"] = genreName;
			newObj["movies"] = [movieObj]; 
			arr.push(newObj);

// If the genre exists: searches if the movie exists.
		} else if (genreFound > 0) {
// searches the object of the genre for the movie to be added.
			var index = 0;
			arr.forEach(function (searchGenre) {
				if (searchGenre.genre == genreName) {
				index = arr.indexOf(searchGenre);
				}
			})

				var movieFound = 0;
			arr[index].movies.forEach(function(titles) {
				if (titles.title == movieObj.title) {
					movieFound = movieFound + 1;
				}
			})

				if (movieFound === 0) {
					arr[index].movies.push(movieObj);
				} else {
			return "the movie " + movieObj.title + " is already in the database!";	
			}
		}
}


module.exports ={
    moviesDB
}