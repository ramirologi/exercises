var Universe = function (value, destination) {

		this.matter = {
							total: 0,
							
							destroy: num => {
								if(num > 0) {
									this.matter.total -= num;
									this.energy.total += num;
								} else {
									this.matter.total += num;
									this.energy.total -= num;
								}
							},

							create: num => {
								if(num > 0) {
									this.matter.total -= num;
									this.energy.total += num;
								} else {
									this.matter.total += num;
									this.energy.total -= num;
								}
							},
		}

		this.energy = {
							total: 0,
			
							destroy: num => {
								if(num > 0) {
									this.matter.total -= num;
									this.energy.total += num;
								} else {
									this.matter.total += num;
									this.energy.total -= num;
								}
							},

							create: num => {
								if(num > 0) {
									this.matter.total -= num;
									this.energy.total += num;
								} else {
									this.matter.total += num;
									this.energy.total -= num;
								}
							},
		}

		if(value !== undefined) {
			if (destination === "matter") {
				this.matter.total = value;
			} else {
				this.energy.total = value;
			}
		}

};

module.exports = {
    Universe
}
