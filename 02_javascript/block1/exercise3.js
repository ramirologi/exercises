var date_of_birth = 1987;
var future_year = 2030;

var ageCalc = function(date_of_birth, future_year){
    return future_year - date_of_birth;
}
module.exports = {
    ageCalc,
    date_of_birth,
    future_year,
}