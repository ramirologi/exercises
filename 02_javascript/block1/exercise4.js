var age = 32;
var end_age = 90;
var teas_day = 2;

var howManyTeas = function(age, end_age, teas_day){
    return (end_age - age) * teas_day * 365;
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}