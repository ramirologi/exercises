var dob = 1987;
var now = 2019;

var howManyDays = function (dob, now){
	var days = (now - dob) * 365;

    return "you have lived for " + days + " days already!";
}
module.exports = {
    now, dob, howManyDays
}