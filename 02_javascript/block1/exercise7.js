var a = 3;

var isEven = function(a) {
	var result = a % 2;
	return result === 0 ? true : false;
}

module.exports = {
    isEven
}