var age = 32;
const minAge = 15;

var checkAge = function (age, minAge){
    return age > minAge;
}

module.exports = {
    checkAge,
    age,
    minAge
}