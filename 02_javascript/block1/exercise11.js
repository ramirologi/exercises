var celsius = 39;
var fahrenheit = 102;

var toCelsius = function (fahrenheit){
    return Math.round((fahrenheit - 32) * 5 / 9);
}
var toFahr = function (celsius){
    return Math.round((celsius * 9 / 5) + 32);
}

module.exports = {
    celsius, 
    fahrenheit, 
    toCelsius, 
    toFahr,
}