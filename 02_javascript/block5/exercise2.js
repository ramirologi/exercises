function calc (num1, num2, operator) {
	
	if (operator === "+") {
		return num1 + num2;
	} else if (operator === "-") {
		return num1 - num2;
	} else if (operator === "*") {
		return num1 * num2;
	} else if (operator === "/") {
		return num1 / num2;
	} else if (num2 === "+" || num2 === "-" ||num2 === "*" ||num2 === "/") {
		return num1;
	}

}


 module.exports = {
     calc
 }