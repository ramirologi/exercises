function tellAge (month,day,year) {

	
	var birthday = new Date(year, (month - 1), day);
	var now = new Date();
	var timeDiff = now - birthday;
	var years = Math.floor(timeDiff / 1000 / 60 / 60 / 24 /365);
	var days = Math.floor(timeDiff / 1000 / 60 / 60 / 24);

	if(years > 1) {
		return `You are ${years} years old`;
	} else if (years < 0) {
		return "You can't be born in the future!"
	} else if (years = 1) {
		return `You are 1 year old`;
	} else {
		return `You are ${days} days old`;
	}
}

module.exports = {
    tellAge
}

