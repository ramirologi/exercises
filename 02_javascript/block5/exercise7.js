function stringChop (str, size) {
	
	var chunk = "";
	var result = "";

	for (var i = 0; i < str.length; i++) {
		
		chunk += str[i];

		if (chunk.length % size === 0) {
			result += chunk + ",";

		chunk = "";

		}

	}

	result += chunk

	return result;

}

module.exports = {
    stringChop
}