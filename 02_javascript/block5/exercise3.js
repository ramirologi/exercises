function filter (arr, dataType, minLength) {

	var newArr = [];

	for (var i = 0; i < arr.length; i++) {
		if (typeof arr[i] !== dataType && arr[i].length >= minLength) {
			newArr.push(arr[i]);
		}
	}

	return String(newArr);
}

module.exports = {
    filter
}