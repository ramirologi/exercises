function checkAge ( month , day ) {

	if(month > 12 || day > 31 || month === 0 || day === 0) {
		return "Error invalid data provided";
	} else {

		var now = new Date();
		var actualMonth = now.getMonth() + 1;
		var actualDay = now.getDate();

		if(actualDay === day && actualMonth === month) {
			return "happy birthday!";
		} else if (actualMonth > month || (actualMonth === month && actualDay > day)) {
			return "Sorry your birthday is passed for this year";
		} else if (actualMonth === month && actualDay < day) {
			var leftDays = day - actualDay;
			return `your birthday will be in ${leftDays} days`;
		} else if ((actualMonth + 1) === month) {
			var remindingDays = 30 - actualDay;
			var finalDays = remindingDays + day;
			return `there are ${finalDays} days until your birthday`;
		} else {
			var leftMonths = month - actualMonth;
			return `your birthday will be in ${leftMonths} months and ${day} days`
		}
	}

}

module.exports = {
    checkAge
}