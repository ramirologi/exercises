var currenciesDB = [];

function dinamicConverter (arg1, currencyArr, curr) {
	
	addedItems = 0;
	var currencies = [];

	if (arg1 === "add") {

		if (currenciesDB.length === 0) {
			addedItems = addedItems + 1;
			currenciesDB.push(currencyArr);
		} else {

			for (var i = 0; i < currenciesDB.length; i++) {
				currencies.push(currenciesDB[i][0]);
			}

			if (currencies.includes(currencyArr[0])) {
				return "invalid data provided!";
			} else {
				addedItems = addedItems + 1;
				currenciesDB.push(currencyArr);
			}
		}

	return addedItems;	
	}	

	var vals = [];
	for (var i = 0; i < currenciesDB.length; i++) {
	currencies.push(currenciesDB[i][0]);
	vals.push(currenciesDB[i][0]);
	}
	
	if (arg1 === "convert") {
		if (typeof curr !== "string" || !vals.includes(curr)) {
			return "invalid data provided!";
		} else {
		var askedCurr = 0;
		currenciesDB.forEach(function (arr) {
			if (arr[0] == currencyArr[0]) {
				askedCurr = arr[1];
			}
		});
		var otherCurr = 0;
		currenciesDB.forEach(function (arr2) {
			if (arr2[0] == curr) {
				otherCurr = arr2[1];
			}
		});

		var result = currencyArr[1] * askedCurr / otherCurr;
		}

	return result;
	}
}


module.exports ={
    currenciesDB, dinamicConverter
}