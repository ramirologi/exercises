function strCut (str, index1, index2) {
	
	var newString = "";

	for (var i = 0; i < str.length; i++) {
		if (i !== index1 && i !== index2) {
					newString += str[i];
		}
	}

return newString;

}

module.exports = {
    strCut
}