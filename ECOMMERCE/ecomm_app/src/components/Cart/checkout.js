import React from 'react'
import axios from 'axios';

// import stripe checkout first
import StripeCheckout from 'react-stripe-checkout';


// that's your publishable key obtained from stripe, for testing use testing key
const STRIPE_PUBLISHABLE = 'pk_test_zYbqVOIDzAt5rkAS6jF7SE1U000O4TWDH6';

// this will be the route to hit on the server after getting token from stripe
const PAYMENT_SERVER_URL = ' http://localhost:8080/purchase';


const CURRENCY = 'EUR';

const fromEuroToCent = amount => amount * 100;

const successPayment = data => {
	alert('Payment Successful');
	console.log(data);
};

const errorPayment = data => {
	alert('Payment Error');
	console.log(data);
};

const onToken = (amount, description) => token =>
axios.post(PAYMENT_SERVER_URL,
{
	description,
	source: token.id,
	currency: CURRENCY,
	amount: fromEuroToCent(amount)
})
.then(successPayment)
.catch(errorPayment);

const Checkout = ({ name, description, amount, label }) =>
<StripeCheckout
name={name}
description={description}
amount={fromEuroToCent(amount)}
token={onToken(amount, description)}
currency={CURRENCY}
stripeKey={STRIPE_PUBLISHABLE}
label = {label}
/>

export default Checkout;
