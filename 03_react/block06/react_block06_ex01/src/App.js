import React, { Component } from 'react';
import NavBar from './components/NavBar';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Gallery from './components/Gallery';

class App extends Component {

  state = {
    currentPage: "home",
  }

  changePage = currentPage => 
    this.setState({currentPage:currentPage.toLowerCase()})

  render () {

    let { currentPage } = this.state;
    let current;

    if(currentPage === "home") {
      current = <Home />;
    } if (currentPage === "about") {
      current = <About />;
    } if (currentPage === "contact") {
      current = <Contact />;
    } if (currentPage === "gallery") {
      current = <Gallery />;
    }

    return (
        
        <div className="App">
            <NavBar 
              changePage = {this.changePage}
            />
            {current}
        </div>
    );
  }
}

export default App;
