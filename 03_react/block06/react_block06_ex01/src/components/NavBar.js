import React, { Component } from 'react';

class NavBar extends Component {

  render () {

    return (
        
        <div className="App">
            <ul 
            style={navStyle} 
            onClick={(e)=>this.props.changePage(e.target.textContent)}>
                <li>Home</li>
                <li>About</li>
                <li>Contact</li>
                <li>Gallery</li>
            </ul>
        </div>
    );
  }
}

const navStyle = {
    display: "flex",
    justifyContent: "space-between",
    listStyle: "none",
    color: "white",
    backgroundColor: "black",
    padding: 0,
    margin: 0,
    height: "60px",
    alignItems: "center",
}

export default NavBar;