import React, { Component } from 'react';
import Button from './Button';


class Input1 extends Component {

	state = {
		email: ""
	}

	handleEmailChange = e => {
		this.setState({email:e.target.value})
	}

 	render() {
		return (

			<div>
				<input placeholder="email" onChange ={this.handleEmailChange} email ="email" value={this.state.email}/>
				<button onClick={()=>this.props.getEmail(this.state.email)}>Submit</button>
			</div>

		)
	}
}

export default Input1