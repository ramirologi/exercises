import React, { Component } from 'react';


class Input2 extends Component {

	state = {
		password: ""
	}
	
	handlePasswordChange = e => {
		this.setState({password:e.target.value})
	}

	render() {
		return (

			<div>
				<input placeholder= "password" onChange ={this.handlePasswordChange} password ="password" value ={this.state.password}/>
				<button onClick={()=>this.props.getPassword(this.state.password)}>Submit</button>
			</div>

		)
	}
}

export default Input2;

