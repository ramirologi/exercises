import React, { Component } from 'react';
import Input1 from './Input1';
import Input2 from './Input2';
import Button from './Button';



class App extends Component {

  state = {
  }

  getEmail = email => {
    this.setState({email});
  }

  getPassword = password => {
    this.setState({password})
  }

  render() {
    return(
        <div>  
            <Input1 getEmail ={this.getEmail}/>   
            <Input2 getPassword ={this.getPassword}/>
            <h1>{this.state.email} - {this.state.password}</h1>
        </div>
    )
  }
}

export default App;
