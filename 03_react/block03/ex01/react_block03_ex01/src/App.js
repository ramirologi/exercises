import React from 'react';


class App extends React.Component {

  state = {
    data: ''
  }

  handleChange = e => {
      this.setState({data: e.target.value})
  }

  render() {
    return (
      
      <div className="App">



        <input onChange={this.handleChange} 
        name= "data" value= {this.state.data}/>
        <h1>{this.state.data}</h1>

        
      </div>

    );
  }
}

export default App;
