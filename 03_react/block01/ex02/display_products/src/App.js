import React, { Component } from 'react';
import "./App.css";

class App extends Component {

  state = {
          products: [
                      {
                        product    : 'flash t-shirt',
                        price      :  27.50,
                        category   : 't-shirts',
                        bestSeller :  false,
                        image      : 'https://www.juniba.pk/wp-content/uploads/2018/02/the-flash-distressed-logo-t-shirt-black.png',
                        onSale     :  "On Sale"
                      },
                      {
                        product    : 'batman t-shirt',
                        price      :  22.50,
                        category   : 't-shirts',
                        bestSeller :  true,
                        image      : 'https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png',
                        onSale     :  ""
                      },
                      {
                        product    : 'superman hat',
                        price      :  13.90,
                        category   : 'hats',
                        bestSeller :  true,
                        image      : 'https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg',
                        onSale     :  ""
                      }
          ],
          bestSeller: [],
  }

  render () {

  this.state.products.map( ( obj , i ) => {
    if(obj.bestSeller === true) {
      this.state.bestSeller.push(obj)
    };
  });

  return (
    <div>

      <header className="extras">
        This is the header...
      </header>

      <h1>BEST SELLERS</h1>
      {this.state.bestSeller.map((obj, i) =>  <h1 key = {i}>
        <h1>{obj.product}</h1>
        <img src={obj.image}/>
        <div className="price">{obj.price}</div>
        {obj.onSale}
        </h1>)
      }

       <h1>ALL PRODUCTS</h1>
      {this.state.products.map((obj, i) =>  <h1 key = {i}>
        <h1>{obj.product}</h1>
        <img src={obj.image}/>
        <div className="price">{obj.price}</div>
        {obj.onSale}
        </h1>)
      }

      <footer className="extras">
        This is the footer...
      </footer>

    </div>
  );
}
}

export default App;
