import React from 'react';


function App() {
  var names = ['t-shirts', 'hats', 'shorts', 'shirts', 'pants', 'shoes'];
  return (
    <div>
      {names.map((name, i) => <h1 key = {i}>{name}</h1>)}
    </div>
  );
}

export default App;
