import React, { Component } from 'react';
import './App.css'

class App extends Component {

   state = {
      username: '',
      password: '',
      submit: false,
      emailOk: false,
      passwordOk: false,
      users: [
         {
            email: "ramiro@ramiro.com",
            password: "1234"
         },
         {
            email: "ramiro2@ramiro2.com",
            password: "12345"
         },
         {
            email: "ramiro3@ramiro3.com",
            password: "123456"
         },
      ],
   }

   handleEmailChange = e => {
      e.preventDefault();
      this.setState({username:e.target.value});
   }

   handlePasswordChange = e => {
      e.preventDefault();
      this.setState({password:e.target.value});
   }

   handleSubmit = e => {
      e.preventDefault();
      this.setState({submit:true});
      this.state.users.map ( ( obj , i ) => {
         if(obj.email === this.state.username) {
               if(obj.password === this.state.password) {
                  this.setState({passwordOk:true , emailOk:true});
               }
         } 
      });
      setTimeout( () => this.setState({submit:false}), 4000);
   }

   render() {

      let message;

      if(this.state.submit === true && this.state.emailOk === true && this.state.passwordOk === true) {
         message = <h3 className="green">Correct!</h3>
      } else if (this.state.submit === false && this.state.emailOk === false && this.state.passwordOk === false){
         message = <h3></h3>
      } else {
         message = <h3 className="red">Invalid data!</h3>
      }
      

      return (

         <div>
            <form>
               <input type="email" placeholder="email" onChange={this.handleEmailChange}/>
               <input type="password" placeholder="password" onChange={this.handlePasswordChange}/>
               <button onClick={this.handleSubmit}>Submit</button>
               {message}
            </form>
         </div>
      );
   }
}

export default  App

