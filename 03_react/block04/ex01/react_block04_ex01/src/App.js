import React, { Component } from 'react';


class App extends Component {

  state = {
    number: 0,
    counter: 0,
  }

  handleClick = () => {
      this.setState({[this.state.number]: this.state.number++})
      if (this.state.number % 2 !== 0) {
        this.state.counter++
      }
    }

  render() {

    return (
      <div className="App">
        
        <div>{this.state.counter}</div>
        <button onClick={this.handleClick}>+</button>

      </div>
    );
  }
}

export default App;
