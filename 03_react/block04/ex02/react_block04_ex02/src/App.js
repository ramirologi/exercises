// ============================= App.js ================================ //

import React, { Component } from 'react';
import Input from './Input';

class App extends Component {

  state = {
        inputValueE: 0,
        inputValueU: 0,
        apiRate: 0
      }
  
  componentDidMount() {
    var rate = [];
    fetch('https://api.exchangeratesapi.io/latest')
      .then(res => res.json())
      .then(json => {
        rate.push(json)
        this.setState({apiRate: rate[0].rates.USD})
      })
    
  }

  handleChangeE = e => {
    this.setState({inputValueE: e.target.value})
  }

  handleChangeU = e => {
    this.setState({inputValueU: e.target.value})
  }

  convertorE = apiRate => {
    return this.state.inputValueE * this.state.apiRate;
  }

  convertorU = apiRate => {
    return this.state.inputValueU / this.state.apiRate;
  }

  render() {

    return(
      <div>
        <h1>
          <input placeholder="Euro to USD" onChange={this.handleChangeE}/>
          <Input convertorE={this.convertorE()}/>
          <input placeholder="USD to Euro" onChange={this.handleChangeU}/>
          <h1>
            {this.convertorU()}
          </h1>
        </h1>
      </div>
    )
  }
}

export default App;


