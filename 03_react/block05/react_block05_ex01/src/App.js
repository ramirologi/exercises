import React, { Component } from 'react';

class App extends Component {

  state = {
    category: "",
    categoryMatch: [],
    cart: [
      {
        name: 'patata',
        price: '1€',
        quantity: 0,
        category: "vegetable",
      },
      {
        name: 'banana',
        price: '1€',
        quantity: 0,
        category: "fruit",
      },
      {
        name: 'fresa',
        price: '1€',
        quantity: 0,
        category: "fruit",
      },
      ]
  }

  handleChange = e => {
    this.setState({category:e.target.value});
 } 

  handleClick = () => {
    var arr = [];
    this.state.cart.forEach( function (product) {
      if(product.category === this.state.category) {
        arr.push(product);
        this.setState({categoryMatch: arr});
      }
    })
  }


  render() {

    let render;

    if(this.state.category === "") {
      render = this.state.cart.map( ( obj , i )  => <ul><li>{obj.name}</li></ul>);
    } else {
      render = this.state.categoryMatch.map( ( obj , i )  => <ul><li>{obj.name}</li></ul>);
    }

    return (
      <div className="App">
        <form>
          <input placeholder="write a category" onChange={this.handleChange}></input>
          <button onClick={this.handleClick}>Show products</button>
        </form>
        {render}
      </div>
    );
  }
}

export default App;
