import React, { Component } from 'react';
import './App.css';


class App extends Component {

  state = {
    email: "",
    password: "",
    confirmPassword: "",
    submit: false,
  }

  handleEmail = e => this.setState({email:e.target.value});
  handlePassword = e => this.setState({password:e.target.value});
  handleConfirmPassword = e => this.setState({confirmPassword:e.target.value});

  handleSubmit = () => {
    this.setState({submit:true});
    setTimeout( () => this.setState({submit:false}), 2000);
  }

  render() {

    let render;

    let count = 0;
    for(var i = 0; i < this.state.email.length; i++) {
      if(this.state.email[i] === "@") {
        count++
      }
    }

    if (this.state.submit === true) {
      if (count !== 1) {
        render = <h3 className="red">Please, provide a valid email.</h3>;
      } else if (this.state.password.length < 8) {
        render = <h3 className="red">Password must ahve at least 8 characters.</h3>;
      } else if (this.state.password !== this.state.confirmPassword) {
        render = <h3 className="red">Passwords should match.</h3>;
      } else {
        render = <h3 className="green">Successfully registered</h3>;
      }
    }

    return (
      <div className="App">
        <input type="email" placeholder="email" onChange={this.handleEmail}/>
        <input type="password" placeholder="password" onChange={this.handlePassword}/>
        <input type="password" placeholder="confirm password" onChange={this.handleConfirmPassword}/>
        <button onClick={this.handleSubmit}>Submit</button>
        {render}
      </div>
    );
  }
}

export default App;
