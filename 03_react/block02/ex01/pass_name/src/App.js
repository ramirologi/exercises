import React from 'react'
import Child from './Child'

class App extends React.Component {
  render() {
    let myname = "Rami";
    return(

      <div>
        <Child myname = {myname}/>
      </div>

    )
  }
}

export default App;