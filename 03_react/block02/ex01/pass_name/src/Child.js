import React from 'react'

class Child extends React.Component {
	render() {
		return(
			<h1>Hello {this.props.myname} i am a child component!</h1>
		)
	}
}

export default Child;