import React from 'react'

class Names extends React.Component {

loopNames = () => (
	this.props.names.map((name, i) => name)
	)

	render() {

		return(
			<div>
				{this.loopNames()}
			</div>
		)

	}

}

export default Names;