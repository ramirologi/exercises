import React from 'react';
import Names from './Names';
import Lastnames from './Lastnames';

class App extends React.Component {
  
  render() {

    let names = ["Rami ","Remy ","Javi ","Yannick ","Juampi "];
    let surenames = ["Lopez ","Nose ","Vierich ","Padin ","Loza "];

    return (

      <div className="App">
        <Names names = {names}/>
        <Lastnames surenames = {surenames}/>
      </div>

    );
  }
}

export default App;
