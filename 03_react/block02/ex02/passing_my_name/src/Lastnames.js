import React from 'react';

class Lastnames extends React.Component{

	loopLastnames = () => (
		this.props.surenames.map((surename, i) => surename)
		)

	render() {

		return(

			<div>
				{this.loopLastnames()}
			</div>

		)

	}

}

export default Lastnames;