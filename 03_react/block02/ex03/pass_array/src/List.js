import React from 'react'
import Item from './Item'

class List extends React.Component {

	loop = () => (
		this.props.arr.map((number, i) => number)
		)

	render() {

		return(
			<div>
				<Item loop= {this.loop}/>	
			</div>
		)
	}
}

export default List; 