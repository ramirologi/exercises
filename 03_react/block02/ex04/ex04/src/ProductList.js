import React, { Component } from 'react';
import SingleProduct from './SingleProduct'

class ProductList extends Component {
  
	loop = () => (
  		this.props.products.map((product, i) => (
  			<SingleProduct key ={i} list ={product} />
  		))
  	)

  render() {

    return (
      <div className="ProductList">
        {this.loop()}
      </div>
    );
  }
}

export default ProductList;