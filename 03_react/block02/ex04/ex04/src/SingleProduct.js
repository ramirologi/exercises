import React, { Component } from 'react';

class SingleProduct extends Component {
  render() {
    return (
      <div className="SingleProduct">
        <h1>{this.props.list.name} - {this.props.list.price}</h1>
      </div>
    );
  }
}

export default SingleProduct;