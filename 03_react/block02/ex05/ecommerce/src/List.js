import React from 'react';
import Item from './Item';

class List extends React.Component {
  
  loop = () => (
    this.props.products.map((product, i) => 
    <h1>{product.name} {product.price} {product.size}</h1>
    )
    )

  render() {

    return (

      <div>
        <Item loop={this.loop()}/>
      </div>

    );
  }
}

export default List;