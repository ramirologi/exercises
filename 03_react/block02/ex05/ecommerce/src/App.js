import React from 'react';
import List from './List';

class App extends React.Component {

    render() { 

    var products = [{name: "Chalkd Markers",size: "6mm", price: "$5.99"},
          {name: "Zen Blackboard",size: "400x800",price: "$69.00"},
          {name: "Counter top talker",size: "A4 & A3",price: "$39.90"},
          {name: "Framed Chalkboard",size: "700mm x 900mm",price: "$259.00"}
          ];

    return (
      <div>
        <List products= {products}/>
      </div>
    )

  }

}

export default App;
