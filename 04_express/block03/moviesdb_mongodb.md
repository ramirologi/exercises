EXERCISE
//=====>   /genres/add        POST            // add new genre to DB
//=====>   /genres/remove/    POST            // remove genre from DB
//=====>   /genres/update     POST            // update genre
//=====>   /genres            GET             // GET all genres
//=====>   /genres/:genre     GET             // GET one genre by name or id 
//=====>   /movies/add        POST            // add new movie to DB
//=====>   /movies/remove     POST            // remove movie from DB
//=====>   /movies/update     POST            // update movie
//=====>   /movies            GET             // get all movies
//=====>   /movies/:movie     GET             // get one movie by title or id 
//=====>   /movies/:genre     GET             // get all movies that belongs to a specific genre

***Your solution goes to the current folder***