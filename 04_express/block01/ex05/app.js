var express = require('express');
var app = express ();
var port = 3001;

var accounts = [];

app.listen(port, ()=> {
    console.log(port);
})

app.get('/:something', ( req , res ) => {
    res.send("404 resource not found");
})

app.get('/', ( req , res ) => {
    res.send("home");
})


app.get('/account/new/:accountID/:amount', ( req , res ) => {
    var accountID = req.params.accountID;
    var amount = Number(req.params.amount);
    var count = 0;
    accounts.forEach( function ( obj , i ) {
        if(obj.id === accountID) {
            count++;
        }
    });
        if(count === 0) {
            accounts.push({id: accountID, amount: amount});
            res.send(`account nr ${accountID} created with ${amount} euros`);
        } else {
            res.send(`the account nr ${accountID} already exists`);
        };
    
});

app.get('/:accountID/withdraw/:amount', ( req , res ) => {
    var accountID = req.params.accountID;
    var amount = Number(req.params.amount);
    accounts.forEach( function ( obj , i ) {
        if(obj.id === accountID) {
            obj.amount -= amount;
        }
    });
    res.send(`${amount} euros taken from account nr ${accountID}`);
})

app.get('/:accountID/deposit/:amount', ( req , res ) => {
    var accountID = req.params.accountID;
    var amount = Number(req.params.amount);
    accounts.forEach( function ( obj , i ) {
        if(obj.id === accountID) {
            obj.amount += amount;
        }
    });
    res.send(`${amount} euros added to account nr ${accountID}`);
})

app.get('/:accountID/balance', ( req , res ) => {
    var accountID = req.params.accountID;
    accounts.forEach( function ( obj , i ) {
        if(obj.id === accountID) {
            var def = obj.amount;
            res.send(`The balance of account nr ${accountID} is ${def} euros`);
        }
    });
})

app.get('/:accountID/delete', ( req , res ) => {
    var accountID = req.params.accountID;
    accounts.forEach( function ( obj , i ) {
        if(obj.id === accountID) {
            var index = accounts.indexOf(obj);
            accounts.splice(index, 1);
            res.send(`Account nr ${accountID} deleted`);
        }
    });
})




// | URL (String)                    | Return (String)                                  | Comments                                                 |
// | ------------------------------- | ------------------------------------------------ | -------------------------------------------------------- |
// | /account/new/:accountID/:amount | account nr :accountID created with :amount euros | GET; it must be unique no matter the number of calls    |
// | /:accountID/withdraw/:amount    | :amount euros taken from account nr :accountID   | GET; if :accountID not found return "Account not found" |
// | /:accountID/deposit/:amount     | :amount euros added to account nr :accountID     | GET; if :accountID not found return "Account not found" |
// | /:accountID/balance             | The balance of account nr :accountID is ## euros | GET; if :accountID not found return "Account not found"  |
// | /:accountID/delete              | Account nr :accountID deleted                    | GET; if :accountID not found return "Account not found" |
// | /*                              | 404 resource not found                           | What to do in case we match anything else                |
