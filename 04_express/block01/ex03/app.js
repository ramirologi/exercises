const express = require('express');
const app = express();
const port = 3001;

app.listen(port, () => {
    console.log(`Server is running in port ${port}`);
})


var language = [
                    {
                        language: "NL",
                        phrase: "Hallo Wereld"
                    },
                    {
                        language: "HI",
                        phrase: "नमस्ते दुनिया"
                    },
                    {
                        language: "FR",
                        phrase: "Bonjour le monde"
                    },
                    {
                        language: "ES",
                        phrase: "Hola Mundo"
                    },
                    {
                        language: "IT",
                        phrase: "Ciao Mondo"
                    },
                    {
                        language: "CH",
                        phrase: "你好，世界"
                    },
                    {
                        language: "JP",
                        phrase: "こんにちは世界"
                    },
                    {
                        language: "AR",
                        phrase: "مرحبا بالعال"
                    },
                    {
                        language: "EN",
                        phrase: "Hello world"
                    },
]


app.get('/', ( req , res ) => {
    res.send('Hello world')
})


app.get('/:lang/', ( req , res ) => {
    var lang = req.params.lang;
    language.map( ( obj , i ) => {
        if(obj.language == lang) {
            res.send(`${obj.phrase}`)
        }
    })
    
})

app.get('/:lang/:phrase', ( req , res ) => {
    var lang = req.params.lang;
	var phrase = req.params.phrase;
	language.map( ( obj , i ) => {
		if(phrase === "remove") {
            var index = language.indexOf(obj);
            language.splice(index , 1);
			res.send(`${lang} removed`)
		} else if(obj.language == lang) {
            res.send(`${obj.phrase}`)
        } else {
            var newPhrase = phrase.replace('_',' ');
            language.push({language: lang, phrase: newPhrase});
            res.send(`${lang} added with message ${newPhrase}`)
        }
    })
})